from flask import *
import json

app = Flask("test")


def add(a,b):
	return a + b

@app.route("/")
def hello():
	return "Hello everyone, how you doin' !!!"

@app.route("/sum")
#two params: a & b
#Ex.: .../sum?a=4&b=8
def sum():
	if 'a' in request.args:
		a = int(request.args['a'])
	else:
		abort(400, "parameter \"a\" is missing")
	if 'b' in request.args:
		b = int(request.args['b'])
	else:
		abort(400, "parameter \"b\" is missing")
	result = add(a,b)
	return json.dumps({'rep':result, 'msg':"the sum of " + str(a) + " + " + str(b) + " is " + str(result)})



if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5555)
